class Sale < ActiveRecord::Base
  belongs_to :customer

  paginates_per 50
max_paginates_per 100
  def amount_with_tax
    self.amount + self.tax
  end
end
